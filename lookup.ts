import axios from 'axios';
require('axios-debug-log');
import { APIGatewayProxyResult } from 'aws-lambda';
import Debug from 'debug';
const debug = Debug('barcode-aws-lambda');

const apiRootUrl = "https://api-na.hosted.exlibrisgroup.com/almaws/v1";

export function validBarcode(barcode: string): boolean {
  const barcodeRegex = /^(?:(?:[as]\d{9,11})|(?:alma\d{5,6})|(?:asu\d{5,28}))$/i;
  return barcodeRegex.test(barcode);
}

export function validMmsId(mms_id: string): boolean {
  const mmsIdRegex = /^\d{15,}$/;
  return mmsIdRegex.test(mms_id);
}

export async function mmsIdFromBarcode(barcode: string): Promise<string> {
  debug(`In mmsIdFromBarcode('${barcode}')`);
  try {
    const url = apiRootUrl + "/items";
    const headers = {
      'Content-Type': 'application/xml',
      'Accept': 'application/json',
      'Authorization': 'apikey ' + process.env.ALMA_BIB_API_KEY
    };
    const r = await axios.get(url, {
      headers: headers,
      params: {
        item_barcode: barcode
      }
    });
    // debug(`response: ${JSON.stringify(r.data, null, 2)}`);
    return r.data.bib_data.mms_id;
  } catch (err) {
    debug("Barcode " + barcode + " not found or other error on lookup.");
    return null;
  }
}

export async function holdingIdsFromMmsId(mms_id: string): Promise<string[]> {
  debug(`In holdingIdsFromMmsId('${mms_id}')`);
  try {
    const url = apiRootUrl + "/bibs/" + mms_id + "/holdings";
    const headers = {
      'Content-Type': 'application/xml',
      'Accept': 'application/json',
      'Authorization': 'apikey ' + process.env.ALMA_BIB_API_KEY
    };
    const r = await axios.get(url, { headers: headers });
    // debug(`response: ${JSON.stringify(r.data, null, 2)}`);
    return r.data.holding.map(h => h.holding_id);
  } catch (err) {
    debug("mms_id " + mms_id + " not found, or other error on lookup.");
    return null;
  }
}

export type ItemsObj = { total_record_count: number, item: Array<any> };

export async function itemsFromHoldingId(mms_id: string,
  holding_id: string, offset = 0, limit = 100): Promise<ItemsObj> {
  debug(`In itemsFromHoldingId('${mms_id}','${holding_id}',${offset})`);
  try {
    const url = apiRootUrl + "/bibs/" + mms_id + "/holdings/" + holding_id + "/items";
    const headers = {
      'Content-Type': 'application/xml',
      'Accept': 'application/json',
      'Authorization': 'apikey ' + process.env.ALMA_BIB_API_KEY
    };
    const params = { limit: limit, offset: offset };
    debug(`parms: ${JSON.stringify(params)}`);
    const r = await axios.get(url, { headers: headers, params: params } );
    // debug(`response: ${JSON.stringify(r.data, null, 2)}`);
    const itemObj = r.data as ItemsObj;
    return itemObj;
  } catch (err) {
    debug(`mms_id ${mms_id} and holding_id ${holding_id} not found or other error on lookup.`);
    return null;
  }
}

export function itemsObjToBarcodes(items: Array<any>): Array<string> {
  return items.map(o => o.item_data.barcode);
}

export async function numberOfAssocBarcodes(barcode: string): Promise<APIGatewayProxyResult> {
  debug(`In numberOfAssocBarcodes('${barcode}')`);
  const response = { statusCode: 404, body: null };
  if (!validBarcode(barcode)) {
    debug("barcode " + barcode + " does not validate against regex");
    return response;
  }

  try {
    const mms_id = await mmsIdFromBarcode(barcode);
    if (!mms_id) return response;
    const hIds = await holdingIdsFromMmsId(mms_id);
    if (!hIds) return response;
    let tot = 0;
    for (const hId of hIds) {
      const { total_record_count, item } = await itemsFromHoldingId(mms_id, hId, 0, 0);
      debug(`total_record_count for holding: ${total_record_count}`);
      tot += total_record_count;
    }
    response.statusCode = 200;
    response.body = tot;
    debug(`Number of barcodes associated with barcode ${barcode}: ${tot}`);
  } catch (err) {
    debug("Barcode " + barcode + " not found or other error on lookup.");
  }
  return response;
}

export async function findAssocBarcodes(barcode: string): Promise<APIGatewayProxyResult> {
  debug(`In findAssocBarcodes('${barcode}')`);
  const response = { statusCode: 404, body: null };
  if (!validBarcode(barcode)) {
    debug("barcode " + barcode + " does not validate against regex");
    return response;
  }

  try {
    const mms_id = await mmsIdFromBarcode(barcode);
    if (!mms_id) return response;
    const hIds = await holdingIdsFromMmsId(mms_id);
    if (!hIds) return response;
    let barcodes: string[] = [];
    for (const hId of hIds) {
      const { total_record_count, item } = await itemsFromHoldingId(mms_id, hId);
      debug(`total_record_count for holding: ${total_record_count}`);
      const bcs = itemsObjToBarcodes(item);
      debug(`barcodes from holding: ${JSON.stringify(bcs)}`);
      barcodes = [...barcodes, ...bcs];
      if (total_record_count > 100) {
        const chunks = Math.ceil((total_record_count - 100) / 100);
        debug(`holding with ${total_record_count} will be called in ${chunks} further rounds`);
        for (let i = 0; i < chunks; ++i) {
          const offset = 100 * (i + 1);
          const { total_record_count, item } = await itemsFromHoldingId(mms_id, hId, offset);
          debug(`total_record_count for holding with offset ${offset}: ${total_record_count}`);
          const bcs = itemsObjToBarcodes(item);
          debug(`barcodes from holding with offset ${offset}: ${JSON.stringify(bcs)}`);
          barcodes = [...barcodes, ...bcs];
        } 
      }
    }
    response.statusCode = 200;
    response.body = JSON.stringify(barcodes);
  } catch (err) {
    debug("Barcode " + barcode + " not found or other error on lookup.");
  }
  return response;
}

export async function lookupByBarcode(barcode: string): Promise<APIGatewayProxyResult> {
  debug(`In lookupByBarcode('${barcode}')`);
  const response = { statusCode: 404, body: null };
  if (!validBarcode(barcode)) {
    debug("barcode " + barcode + " does not validate against regex");
    return response;
  }

  // const bc = 'A15017621529'; // legit barcode example 
  try {
    const url = apiRootUrl + "/items";
    const headers = {
      'Content-Type': 'application/xml',
      'Accept': 'application/json',
      'Authorization': 'apikey ' + process.env.ALMA_BIB_API_KEY
    };
    const r = await axios.get(url, {
      headers : headers,
      params: {
        item_barcode: barcode,
        view: 'label'
      }
    });
    // debug(`response: ${JSON.stringify(r.data, null, 2)}`);
    response.statusCode = r.status;
    response.body = JSON.stringify(r.data, null, 2);
    // debug("response:\n" + response.body);
  } catch (err) {
    debug("Barcode " + barcode + " not found or other error on lookup.");
  }
  return response;
}

export async function lookupByMmsId(mmsId: string): Promise<APIGatewayProxyResult> {
  debug(`In lookupByMmsId('${mmsId}')`);
  const response = { statusCode: 404, body: null };
  if (!validMmsId(mmsId)) {
    debug("mms-id " + mmsId + " does not validate against regex");
    return response;
  }

  try {
    const url = apiRootUrl + "/bibs/" + mmsId;
    debug(`lookupByMmsId, GET request to url ${url}`)
    const headers = {
      'Content-Type': 'application/xml',
      'Accept': 'application/json',
      'Authorization': 'apikey ' + process.env.ALMA_BIB_API_KEY
    };
    debug('About to call Alma API')
    const r = await axios.get(url, {
      headers : headers
    });
    response.statusCode = r.status;
    debug(`response.statusCode:`, response.statusCode);
    response.body = JSON.stringify(r.data, null, 2);
  } catch (err) {
    debug("mms-id " + mmsId + " not found, or other error on lookup.");
  }
  return response;
}