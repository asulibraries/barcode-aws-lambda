import { APIGatewayProxyHandler } from 'aws-lambda';
import 'source-map-support/register';
import { lookupByBarcode, lookupByMmsId, findAssocBarcodes, numberOfAssocBarcodes } from './lookup';

export const mmsId: APIGatewayProxyHandler = async (event, _context) => {
  const mmsid = (event && event.pathParameters) ? event.pathParameters.mmsid : null;
  return await lookupByMmsId(mmsid);
};

export const barcode: APIGatewayProxyHandler = async (event, _context) => {
  const bc = (event && event.pathParameters) ? event.pathParameters.bc : null;
  return await lookupByBarcode(bc);
};

export const otherBarcodes: APIGatewayProxyHandler = async (event, _context) => {
  const bc = (event && event.pathParameters) ? event.pathParameters.bc : null;
  return await findAssocBarcodes(bc);
};

export const totalBarcodes: APIGatewayProxyHandler = async (event, _context) => {
  const bc = (event && event.pathParameters) ? event.pathParameters.bc : null;
  return await numberOfAssocBarcodes(bc);
};
