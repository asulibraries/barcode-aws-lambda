# Alma item barcode lookup AWS Lambda

A simple proxy endpoint to hide the read-only API key for Alma BIBs for barcode lookup.

## Deploy

In order to deploy the endpoint:

This program uses the dotenv-flow.  For local testing, and in the Serverless Framework deployment,
this means that environment variables are read from the check-in file `.env`, and overrides
and secrets are set in the not-checked-in file `.env.local`.  Current secrets set in `.env.local`
should always be stored in LastPass for future reference.

Set the read-only Alma API key as a value in `.env.local` which is a file that is excluded for being
checked in from gitignore.  Create `.env.local` in this directory and with this value:
```
ALMA_API_BIB_KEY: <Your Secret API key here.  Do not check in!>
```

AWS credentials and profiles are normally stored in `~/.aws/credentials` and `~/.aws/config`.  For
more information see https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-profiles.html.
Assume that profiles called `slsprod` and `slsdev` have been configured, we can deploy to these
different profiles (could by different accounts or IAM user for each profile) with
```
serverless --aws-profile slsdev
```
or
```
serverless deploy --stage prod --aws-profile slsprod
```

Note: the `--stage` option affects the value of the `stage` in the `provider` section of
`serverless.yml`.  By default, it is set to `dev`.  For production, we recommend setting it to
`prod`.  The only effect of this variable is that the value appears at the root of the API path.
This is useful for deploying a test, dev, or qa version to the same AWS account as the current
production version.  For a production deployment, we recommend choosing `prod`.

The deploy process will return a Lambda Gateway API key such as:
```
api keys:
  gatewayApiKey_dev: 542Dfapgweffasdfsdfo4fasdfp94rsdf
```

It will also return the URL endpoint such as:
```
endpoints:
  GET - https://ba9993jijp.execute-api.us-west-2.amazonaws.com/dev/barcode/{bc}
```

To make a GET request to the URL, you need to include the gatewayApiKey_dev as the value of
the `x-api-key` property in the header.  Using `curl`, this could be done as follows:

```
curl -H 'x-api-key: 542Dfapgweffasdfsdfo4fasdfp94rsdf' https://ba9993jijp.execute-api.us-west-2.amazonaws.com/dev/barcode/A15017621529
```

To test, edit file `.env.local` (or create if doesn't exist) in the root of this project directory
and add an entry for the the AWS Gateway API key returned from the deployment above as variable
`GATEWAY_API_KEY`.  This file is ignored by git.  Enter the deployed root URL as
`DEPLOYED_AT_ROOT_URL`, and ensure `ALMA_BIB_API_KEY`  is set.
Enter the barcode to test as `TEST_BARCODE`.  For
example
```
ALMA_BIB_API_KEY=542Dfapgweffasdfsdfo4fasdfp94rsdf
GATEWAY_API_KEY=pedf3adflefasdflefasdflefSDGAdfsdf
DEPLOYED_AT_ROOT_URL=https://12345ghrsf.execute-api.us-east-2.amazonaws.com/dev
TEST_BARCODE=A15020086978
```

Once the above is done run the test to look up a barcode (edit `test.js` as needed to try a
different barcode) run:
```bash
npm test
```

```
DEBUG=barcode-aws-lambda sls invoke local -f barcode --data '{ "pathParameters": {"bc": "A15020086978"}}'
DEBUG=barcode-aws-lambda sls invoke local -f mmsId --data '{ "pathParameters": {"mmsid": "991022778549703841"}}'
DEBUG=barcode-aws-lambda sls invoke local -f otherBarcodes --data '{ "pathParameters": {"bc": "A15020086978"}}'
DEBUG=barcode-aws-lambda sls invoke local -f totalBarcodes --data '{ "pathParameters": {"bc": "A15020086978"}}'
```


## Monitor

To monitor the Lambda cron job that updates DynamoDB cache every 5 minutes:
```bash
serverless logs --function mmsId -t --aws-profile slsprod --stage prod
```

To list currently deployed functions
```bash
serverless deploy list functions --aws-profile slsprod --stage prod
```
