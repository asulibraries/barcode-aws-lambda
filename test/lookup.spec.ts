import Debug from 'debug';
const debug = Debug('barcode-aws-lambda');
require('axios-debug-log');
import axios from 'axios';
import 'mocha';
import * as chai from "chai";
const expect = chai.expect;
import { mmsIdFromBarcode, holdingIdsFromMmsId, itemsFromHoldingId, lookupByMmsId,
  findAssocBarcodes, numberOfAssocBarcodes, lookupByBarcode } from '../lookup';

describe('lookup', function () {
  const barcode = process.env.TEST_BARCODE;
  const mms_id = process.env.TEST_MMS_ID;
  const holding_id = process.env.TEST_HOLDING_ID;

  describe('find mms-id from barcode', function () {
    this.timeout(5000);
    it('should bib mms-id for a known barcode', async function () {
      const mms_id = await mmsIdFromBarcode(barcode);
      expect(mms_id).to.exist;
      // debug(`Response body:\n${r.body}`);
      debug(`bib mms_id ${mms_id} returned from barcode ${barcode}`);
    });
  });

  describe('find holdings ids from mms-id', function () {
    this.timeout(5000);
    it('should find array of holdings ids from a known mms-id', async function () {
      const holdings_ids = await holdingIdsFromMmsId(mms_id);
      expect(holdings_ids).to.exist;
      expect(holdings_ids.length).to.be.gte(1);
      debug(`holdings ${JSON.stringify(holdings_ids)} returned from mms-id ${mms_id}`);
    });
  });

  describe('find items from mms-id and holding id', function () {
    this.timeout(5000);
    it('should find array of items from a known mms-id and holding id', async function () {
      const items_obj = await itemsFromHoldingId(mms_id, holding_id);
      expect(items_obj.total_record_count).to.be.gte(1);
      expect(items_obj.item.length).to.be.gte(1);
      debug(`items.total_record_count: ${items_obj.total_record_count} returned from mms-id ${mms_id} and holding id ${holding_id}`);
    });
  });

  describe('find number of associated barcodes', function () {
    this.timeout(24900); // 24.9 seconds
    it('should find corret number of related barcodes', async function () {
      const r = await numberOfAssocBarcodes(barcode);
      expect(r.statusCode).to.equal(200);
      expect(r.body).to.exist;
      const tot = JSON.parse(r.body);
      // debug(`Response body:\n${r.body}`);
      debug(`tot: ${tot}`);
      expect(tot).to.be.greaterThan(0);
      debug(`total number of barcodes: ${tot}`);
    });
  });

  describe('find associated barcodes', function () {
    this.timeout(24900); // 24.9 seconds
    it('should find all related barcodes', async function () {
      const r = await findAssocBarcodes(barcode);
      expect(r.statusCode).to.equal(200);
      expect(r.body).to.exist;
      const barcodes = JSON.parse(r.body);
      debug(`Response body:\n${r.body}`);
      debug(`barcodes: ${barcodes}`);
      expect(barcodes.length).to.be.greaterThan(0);
      debug(`total number of barcodes: ${barcodes.length}`);
    });
  });

  describe('calling Alma API directly for known barcode', function() {
    this.timeout(5000);
    it('should find a known book by barcode', async function() {
      const r = await lookupByBarcode(barcode);
      expect(r.statusCode).to.equal(200);
      expect(r.body).to.exist;
      const data = JSON.parse(r.body);
      debug(`data:`, data)
      debug(`found bib_data:\n${JSON.stringify(data.bib_data, null, 2)}`);
      expect(data.bib_data).to.exist;
    });
  });

  describe('calling Alma API directly to find bib by mms-id', function() {
    this.timeout(5000);
    it('should find a known book by mms-id', async function() {
      const r = await lookupByMmsId(mms_id);
      expect(r.statusCode).to.equal(200);
      expect(r.body).to.exist;
      const data = JSON.parse(r.body);
      debug(`found mms_id:\n${JSON.stringify(data.mms_id, null, 2)}`);
      expect(data.mms_id).to.exist;
    });
  });

  describe('calling Alma API through AWS Lambda', function () {
    this.timeout(15000);
    it('should find a known book by mms-id', async function () {
      const url = `${process.env.DEPLOYED_AT_ROOT_URL}/bibs/${mms_id}`;
      debug(`GET ${url}`);
      const { data } = await axios.get(url, {
        headers: {
          'x-api-key': process.env.GATEWAY_API_KEY
        }
      });
      //debug(`result: ${JSON.stringify(data, null, 2)}`);
      expect(data).to.exist;
    });
  });
});
